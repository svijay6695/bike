$('.content').slick({
    slidesToShow: 1,
    arrows: true,
    autoplay: true,
    speed: 1000,
    infinite: true,
    nextArrow: '<div class="slick-right"><img src="assets/right.svg"></div>',
    prevArrow: '<div class="slick-left"><img src="assets/left.svg"></div>',
  });
  